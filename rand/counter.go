package rand

import (
	"regexp"

	"github.com/bxcodec/faker/v3"
)

func HitCount(num int) int {
	hits := 0

	for i := 0; i < num; i++ {
		re := regexp.MustCompile(`accusantium`)

		if re.Match([]byte(faker.Sentence())) {
			hits++
		}
	}

	return hits
}
