package main

import (
	"fmt"
	"log"
	"net/http"
	_ "net/http/pprof"

	"gitlab.com/steveazz/blog/go-performance-tools-cheat-sheet/rand"
)

func main()  {
	http.HandleFunc("/", hitCounter)

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}

func hitCounter(w http.ResponseWriter, r *http.Request) {
	total := 1000000
	hits := rand.HitCount(total)

	_, err := fmt.Fprintf(w, "%d out of %d", hits, total)
	if err != nil {
		http.Error(w, "writing request", 500)
	}
}
